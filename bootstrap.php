<?php

require_once "vendor/autoload.php";

use Doctrine\DBAL\Types\Type;
use Doctrine\ORM\Tools\Setup;
use Doctrine\ORM\EntityManager;
use AstroLab\SolarSystem\Command\AddSolarSystemCommand;
use AstroLab\SolarSystem\SolarSystemDoctrineRepository;
use AstroLab\SolarSystem\Command\ListSolarSystemsCommand;
use Symfony\Component\DependencyInjection\ContainerBuilder;

// custom doctrine types
Type::addType(
    'SolarSystemIdentity',
    'AstroLab\Doctrine\Type\SolarSystemIdentityType'
);
Type::addType(
    'PlanetIdentity',
    'AstroLab\Doctrine\Type\PlanetIdentityType'
);

$isDevMode = true;
$proxyDir = null;
$cache = null;
$useSimpleAnnotationReader = false;
$config = Setup::createXMLMetadataConfiguration(array(__DIR__."/config/xml"), $isDevMode);

// database configuration parameters
$dbParams = [
    'driver' => 'pdo_mysql',
    'user' => 'root',
    'password' => 'astrolab',
    'host' => 'mariadb',
    'port' => 3306,
    'dbname' => 'astrolab',
    'charset' => 'UTF8',
];

// obtaining the entity manager
$entityManager = EntityManager::create($dbParams, $config);

$containerBuilder = new ContainerBuilder();
$containerBuilder->setParameter('entityManager', $entityManager);
$containerBuilder->register('solarSystemRepository', SolarSystemDoctrineRepository::class)
    ->addArgument('%entityManager%');
$containerBuilder->register('listSolarSystemsCommand', ListSolarSystemsCommand::class);
$containerBuilder->register('addSolarSystemCommand', AddSolarSystemCommand::class)
    ->addArgument('%solarSystemRepository%');
