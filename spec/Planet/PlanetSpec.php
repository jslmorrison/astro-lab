<?php

namespace spec\AstroLab\Planet;

use AstroLab\Planet\Planet;
use PhpSpec\ObjectBehavior;

class PlanetSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(Planet::class);
    }

    public function let()
    {
        $this->beConstructedThrough(
            'namedWithMassAndDiameter',
            [
                'Earth',
                5.972E24,
                12742
            ]
        );
    }

    public function it_should_be_identifiable()
    {
        $this->id()->shouldBeAnInstanceOf(\AstroLab\Identity::class);
    }

    public function it_should_be_named()
    {
        $this->name()->shouldReturn('Earth');
    }

    public function it_should_have_mass()
    {
        $this->mass()->shouldBeFloat();
        $this->mass()->shouldReturn(5.972E24);
    }

    public function it_should_have_a_diameter()
    {
        $this->diameter()->shouldBeFloat();
        $this->diameter()->shouldReturn(12742.00);
    }
}
