<?php

namespace spec\AstroLab\Command;

use AstroLab\Command\WelcomeCommand;
use PhpSpec\ObjectBehavior;

class WelcomeCommandSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(WelcomeCommand::class);
    }
}
