<?php

namespace spec\AstroLab\SolarSystem\Message;

use PhpSpec\ObjectBehavior;
use AstroLab\SolarSystem\Message\AddSolarSystemMessage;

class AddSolarSystemMessageSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(AddSolarSystemMessage::class);
    }

    public function let()
    {
        $this->beConstructedWith('The name of the Solar System');
    }

    public function it_should_return_the_name_of_solar_system_to_be_added()
    {
        $this->name()->shouldReturn('The name of the Solar System');
    }
}
