<?php

namespace spec\AstroLab\Command;

use PhpSpec\ObjectBehavior;
use AstroLab\SolarSystem\Command\AddSolarSystemCommand;

class AddSolarSystemCommandSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(AddSolarSystemCommand::class);
    }

    public function let(\AstroLab\SolarSystem\SolarSystemRepository $solarSystemRepository)
    {
        $this->beConstructedWith($solarSystemRepository);
    }
}
