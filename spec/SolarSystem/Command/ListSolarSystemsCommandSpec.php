<?php

namespace spec\AstroLab\Command;

use PhpSpec\ObjectBehavior;
use AstroLab\SolarSystem\Command\ListSolarSystemsCommand;

class ListSolarSystemsCommandSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(ListSolarSystemsCommand::class);
    }

    public function let(\AstroLab\SolarSystem\SolarSystemRepository $solarSystemRepository)
    {
        $this->beConstructedWith($solarSystemRepository);
    }
}
