<?php

namespace spec\AstroLab\SolarSystem\MessageHandler;

use PhpSpec\ObjectBehavior;
use AstroLab\SolarSystem\MessageHandler\AddSolarSystemHandler;

class AddSolarSystemHandlerSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(AddSolarSystemHandler::class);
    }

    public function let(\AstroLab\SolarSystem\SolarSystemRepository $solarSystemRepository)
    {
        $this->beConstructedWith($solarSystemRepository);
    }
}
