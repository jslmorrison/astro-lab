<?php

namespace spec\AstroLab\SolarSystem;

use PhpSpec\ObjectBehavior;
use AstroLab\SolarSystem\SolarSystemDoctrineRepository;

class SolarSystemDoctrineRepositorySpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(SolarSystemDoctrineRepository::class);
    }

    public function it_should_implement_interface()
    {
        $this->shouldImplement(\AstroLab\SolarSystem\SolarSystemRepository::class);
    }

    public function let(\Doctrine\ORM\EntityManager $entityManager)
    {
        $this->beConstructedWith($entityManager);
    }
}
