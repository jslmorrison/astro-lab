<?php

namespace spec\AstroLab\SolarSystem;

use PhpSpec\ObjectBehavior;
use AstroLab\SolarSystem\SolarSystemIdentity;

class SolarSystemIdentitySpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(SolarSystemIdentity::class);
    }

    public function let()
    {
        $this->beConstructedWith(\Ramsey\Uuid\Uuid::uuid4()->toString());
    }

    public function it_should_generate_identity()
    {
        $identity = $this->generate();
        $identity->shouldBeAnInstanceOf(\AstroLab\Identity::class);
        $identity->__toString()->shouldBeString();
    }

    public function it_should_implement_interface()
    {
        $this->shouldImplement(\AstroLab\Identity::class);
    }
}
