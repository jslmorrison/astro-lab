<?php

namespace spec\AstroLab\SolarSystem;

use PhpSpec\ObjectBehavior;
use AstroLab\SolarSystem\SolarSystem;

class SolarSystemSpec extends ObjectBehavior
{
    public function it_is_initializable()
    {
        $this->shouldHaveType(SolarSystem::class);
    }

    public function let()
    {
        $this->beConstructedThrough(
            'named',
            ['The Solar System']
        );
    }

    public function it_should_be_identifiable()
    {
        $this->id()->shouldBeAnInstanceOf(\AstroLab\Identity::class);
    }

    public function it_should_be_named()
    {
        $this->name()->shouldReturn('The Solar System');
    }

    public function it_should_have_collection_of_astronomical_bodies()
    {
        $this->astronomicalBodies()->shouldBeAnInstanceOf(\Doctrine\Common\Collections\Collection::class);
    }

    public function it_should_be_able_to_add_to_astronomical_bodies_collection()
    {
        $astronomicalBody = \AstroLab\Planet\Planet::namedWithMassAndDiameter(
            'Earth',
            5.972E24,
            12742
        );
        $this->astronomicalBodies()->isEmpty()->shouldBe(true);
        $this->addAstronomicalBody($astronomicalBody);
        $this->astronomicalBodies()->isEmpty()->shouldBe(false);
        $this->astronomicalBodies()->count()->shouldReturn(1);
        $this->addAstronomicalBody($astronomicalBody);
        $this->astronomicalBodies()->count()->shouldReturn(1);
        $astronomicalBody = \AstroLab\Planet\Planet::namedWithMassAndDiameter(
            'Mars',
            3.978E30,
            6779
        );
        $this->addAstronomicalBody($astronomicalBody);
        $this->astronomicalBodies()->count()->shouldReturn(2);
    }

    public function it_should_be_able_to_remove_from_astronomical_bodies_collection()
    {
        $astronomicalBody = \AstroLab\Planet\Planet::namedWithMassAndDiameter(
            'Earth',
            5.972E24,
            12742
        );
        $this->addAstronomicalBody($astronomicalBody);
        $this->astronomicalBodies()->count()->shouldReturn(1);
        $this->removeAstronomicalBody($astronomicalBody);
        $this->astronomicalBodies()->isEmpty()->shouldBe(true);
        $this->mass()->shouldReturn(0.00);
    }

    public function it_should_know_its_total_mass()
    {
        $this->mass()->shouldReturn(0.00);
        $astronomicalBody = \AstroLab\Planet\Planet::namedWithMassAndDiameter(
            'Earth',
            5.972E24,
            12742
        );
        $this->addAstronomicalBody($astronomicalBody);
        $this->astronomicalBodies()->count()->shouldReturn(1);
        $astronomicalBody = \AstroLab\Planet\Planet::namedWithMassAndDiameter(
            'Mars',
            6.4185E23,
            6779
        );
        $this->addAstronomicalBody($astronomicalBody);
        $this->astronomicalBodies()->count()->shouldReturn(2);
        $this->mass()->shouldReturn(6.61385E+24);
    }
}
