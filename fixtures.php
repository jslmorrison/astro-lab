<?php

require_once 'vendor/autoload.php';
require_once 'bootstrap.php';

use MyDataFixtures\UserDataLoader;
use Doctrine\Common\DataFixtures\Loader;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\DataFixtures\Executor\ORMExecutor;

$loader = new Loader();
$loader->loadFromFile(__DIR__ . '/src/DataFixtures/AstroLabFixtures.php');

$purger = new ORMPurger();
$executor = new ORMExecutor($entityManager, $purger);
$executor->execute($loader->getFixtures());
