# AstroLab
A command line app to manage astronical bodies in the Solar System.

## Setup
Bring up app with:

```docker-compose up -d```

Update schema with

```docker exec -it astrolab_php php vendor/bin/doctrine orm:schema-tool:create"```

## Fixtures
To preload some fixture data:

```docker exec -it astrolab_php php fixtures.php```

## How to use
```docker exec -it astrolab_php php astro_lab.php```

and follow the on screen instructions.

## Tests
To run php specs and unit tests:

```docker exec -it astrolab_php bash -c "php vendor/bin/phpspec run -f pretty && php vendor/bin/phpunit tests"```