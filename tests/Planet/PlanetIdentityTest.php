<?php

use AstroLab\Identity;
use PHPUnit\Framework\TestCase;
use AstroLab\Planet\PlanetIdentity;

final class PlanetIdentityTest extends TestCase
{
    public function testItCanBeConstructedWithValidString()
    {
        $identity = new PlanetIdentity('e29df9d4-0215-49ed-b23e-ce255aeed063');
        $this->assertInstanceOf(Identity::class, $identity);
        $this->assertEquals('e29df9d4-0215-49ed-b23e-ce255aeed063', $identity);
    }

    public function testItThrowsExceptionWhenConstructedWithNonValidString()
    {
        $this->expectException(InvalidArgumentException::class);
        new PlanetIdentity('bogus-uuid-1');
    }
}
