<?php

use AstroLab\Identity;
use PHPUnit\Framework\TestCase;
use AstroLab\SolarSystem\SolarSystemIdentity;

final class SolarSystemIdentityTest extends TestCase
{
    public function testItCanBeConstructedWithValidString()
    {
        $identity = new SolarSystemIdentity('e29df9d4-0215-49ed-b23e-ce255aeed063');
        $this->assertInstanceOf(Identity::class, $identity);
        $this->assertEquals('e29df9d4-0215-49ed-b23e-ce255aeed063', $identity);
    }

    public function testItThrowsExceptionWhenConstructedWithNonValidString()
    {
        $this->expectException(InvalidArgumentException::class);
        $identity = new SolarSystemIdentity('bogus-uuid-1');
    }
}
