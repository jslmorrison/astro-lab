<?php

namespace AstroLab\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;

/**
 * Class WelcomeCommand
 *
 * @package AstroLab\Command
 */
final class WelcomeCommand extends Command
{
    const LIST_SOLAR_SYSTEMS = 'List Solar Systems';
    const ADD_SOLAR_SYSTEM = 'Add Solar System';

    protected static $defaultName = 'astrolab:welcome';

    protected function configure()
    {
        // stub
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Welcome to the AstroLab');

        $helper = $this->getHelper('question');
        $question = new ChoiceQuestion(
            'Please select option from one of the following:',
            [
                'HAL',
                self::LIST_SOLAR_SYSTEMS,
                self::ADD_SOLAR_SYSTEM,
            ],
            0
        );
        $question->setErrorMessage('Option %s is invalid.');
        $option = $helper->ask($input, $output, $question);

        switch ($option) {
            case self::LIST_SOLAR_SYSTEMS:
                $command = $this->getApplication()
                    ->find('astrolab:solar-systems:list');
                $command->run($input, $output);
                break;
            case self::ADD_SOLAR_SYSTEM:
                $command = $this->getApplication()
                    ->find('astrolab:solar-systems:add');
                $command->run($input, $output);
                break;
            default:
                $io->caution('Sorry Dave. I cant allow you to do that!');
            break;
        }

        return Command::SUCCESS;
    }
}
