<?php

namespace AstroLab\Planet;

use AstroLab\Identity;
use AstroLab\AstronomicalBody;
use AstroLab\Planet\PlanetIdentity;
use AstroLab\SolarSystem\SolarSystemInterface;

/**
 * Class Planet
 *
 * @package AstroLab\Planet
 */
final class Planet extends AstronomicalBody
{
    const TYPE = 'planet';

    private function __construct()
    {
    }

    public static function namedWithMassAndDiameter(
        string $name,
        float $mass,
        float $diameter
    ): self {
        $planet = new self();
        $planet->id = PlanetIdentity::generate();
        $planet->name = $name;
        $planet->mass = $mass;
        $planet->diameter = $diameter;

        return $planet;
    }
}
