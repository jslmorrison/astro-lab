<?php

namespace AstroLab\Planet;

use AstroLab\Identity;
use AstroLab\IdentityTrait;

/**
 * Class PlanetIdentity
 *
 * @package AstroLab\Planet
 */
final class PlanetIdentity implements Identity
{
    use IdentityTrait;
}
