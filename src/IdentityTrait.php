<?php

namespace AstroLab;

use Ramsey\Uuid\Uuid;

/**
 * Trait IdentityTrait
 *
 * @package AstroLab
 */
trait IdentityTrait
{
    private $value;

    public function __construct(string $value)
    {
        if (
            Uuid::isValid($value) === false ||
            Uuid::fromString($value)->getVersion() !== Uuid::UUID_TYPE_RANDOM
        ) {
            throw new \InvalidArgumentException('Invalid UUID string: ' . $value);
        }
        $this->value = $value;
    }

    public static function generate(): self
    {
        $identity = new self(Uuid::uuid4()->toString());

        return $identity;
    }

    public function __toString(): string
    {
        return $this->value;
    }
}
