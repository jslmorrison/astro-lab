<?php

namespace AstroLab\Doctrine\Type;

use Doctrine\DBAL\Types\GuidType;
use AstroLab\SolarSystem\SolarSystemIdentity;
use Doctrine\DBAL\Platforms\AbstractPlatform;

/**
 * Class PlanetIdentityType
 *
 * @package AstroLab\Doctrine\Type
 */
final class PlanetIdentityType extends GuidType
{
    public function getName()
    {
        return 'PlanetIdentity';
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value;
    }
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return PlanetIdentity::generate($value);
    }
}
