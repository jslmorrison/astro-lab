<?php

namespace AstroLab\Doctrine\Type;

use Doctrine\DBAL\Types\GuidType;
use AstroLab\SolarSystem\SolarSystemIdentity;
use Doctrine\DBAL\Platforms\AbstractPlatform;

/**
 * Class SolarSystemIdentityType
 *
 * @package AstroLab\Doctrine\Type
 */
final class SolarSystemIdentityType extends GuidType
{
    public function getName()
    {
        return 'SolarSystemIdentity';
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        return $value;
    }
    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return SolarSystemIdentity::generate($value);
    }
}
