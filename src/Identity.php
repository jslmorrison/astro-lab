<?php

namespace AstroLab;

/**
 * Interface Identity
 *
 * @package AstroLab
 */
interface Identity
{
    public static function generate(): self;
    public function __toString(): string;
}
