<?php

namespace AstroLab\SolarSystem;

use AstroLab\Identity;
use Doctrine\ORM\EntityManager;
use AstroLab\SolarSystem\SolarSystem;
use AstroLab\SolarSystem\SolarSystemRepository;

/**
 * Class SolarSystemDoctrineRepository
 *
 * @package AstroLab\SolarSystem
 */
final class SolarSystemDoctrineRepository implements SolarSystemRepository
{
    private $entityManager;
    
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    
    public function findAll(): array
    {
        $queryBuilder = $this->entityManager
            ->createQueryBuilder()
            ->select('ss')
            ->from('\AstroLab\SolarSystem\SolarSystem', 'ss');

        return $queryBuilder->getQuery()
            ->getResult();
    }

    public function find(Identity $id): ?SolarSystem
    {
        return null; // todo
    }

    public function findByName(string $name): ?SolarSystem
    {
        $queryBuilder = $this->entityManager
            ->createQueryBuilder()
            ->select('ss')
            ->from('\AstroLab\SolarSystem\SolarSystem', 'ss')
            ->where('ss.name = :name')
            ->setParameter('name', $name)
            ->setMaxResults(1);

        return $queryBuilder->getQuery()
            ->getOneOrNullResult();
    }

    public function save(SolarSystem $solarSystem): void
    {
        $this->entityManager
            ->persist($solarSystem);
        $this->entityManager
            ->flush();
    }
}
