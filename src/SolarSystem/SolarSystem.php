<?php

namespace AstroLab\SolarSystem;

use AstroLab\Identity;
use AstroLab\AstronomicalBodyInterface;
use Doctrine\Common\Collections\Collection;
use AstroLab\SolarSystem\SolarSystemIdentity;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class SolarSystem
 *
 * @package AstroLab\SolarSystem
 */
final class SolarSystem
{
    private $id;
    private $name;
    private $astronomicalBodies;
    private $mass = 0;
    private $planets;

    private function __construct()
    {
    }

    public static function named(string $name): self
    {
        $solarSystem = new self();
        $solarSystem->id = SolarSystemIdentity::generate();
        $solarSystem->name = $name;
        $solarSystem->astronomicalBodies = new ArrayCollection();

        return $solarSystem;
    }

    public function id(): Identity
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function astronomicalBodies(): Collection
    {
        return $this->astronomicalBodies;
    }

    public function addAstronomicalBody(AstronomicalBodyInterface $astronomicalBody): void
    {
        if (!$this->astronomicalBodies()->contains($astronomicalBody)) {
            $this->astronomicalBodies()
                ->add($astronomicalBody);
            $astronomicalBody->setSolarSystem($this);
            $this->mass += $astronomicalBody->mass();
        }
    }

    public function removeAstronomicalBody(AstronomicalBodyInterface $astronomicalBody): void
    {
        if ($this->astronomicalBodies()->contains($astronomicalBody)) {
            $this->astronomicalBodies()
                ->removeElement($astronomicalBody);
            $this->mass -= $astronomicalBody->mass();
        }
    }

    public function mass(): float
    {
        return $this->mass;
    }
}
