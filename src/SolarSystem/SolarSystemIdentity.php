<?php

namespace AstroLab\SolarSystem;

use AstroLab\Identity;
use AstroLab\IdentityTrait;

/**
 * Class SolarSystemIdentity
 *
 * @package AstroLab\SolarSystem
 */
final class SolarSystemIdentity implements Identity
{
    use IdentityTrait;
}
