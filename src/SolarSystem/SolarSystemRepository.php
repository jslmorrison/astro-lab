<?php

namespace AstroLab\SolarSystem;

use AstroLab\Identity;

/**
 * Interface SolarSystemRepository
 *
 * @package AstroLab\SolarSystem
 */
interface SolarSystemRepository
{
    public function findAll(): array;
    public function find(Identity $id): ?SolarSystem;
    public function findByName(string $name): ?SolarSystem;
    public function save(SolarSystem $solarSystem): void;
}
