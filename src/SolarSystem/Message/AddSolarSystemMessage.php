<?php

namespace AstroLab\SolarSystem\Message;

/**
 * Class AddSolarSystemMessage
 *
 * @package AstroLab\SolarSystem\Message
 */
final class AddSolarSystemMessage
{
    private $name;

    public function __construct(string $name)
    {
        $this->name = $name;
    }

    public function name(): string
    {
        return $this->name;
    }
}
