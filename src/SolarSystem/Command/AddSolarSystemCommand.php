<?php

namespace AstroLab\SolarSystem\Command;

use Symfony\Component\Messenger\MessageBus;
use Symfony\Component\Console\Command\Command;
use AstroLab\SolarSystem\SolarSystemRepository;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use AstroLab\SolarSystem\Message\AddSolarSystemMessage;
use Symfony\Component\Messenger\Handler\HandlersLocator;
use AstroLab\SolarSystem\MessageHandler\AddSolarSystemHandler;
use Symfony\Component\Messenger\Middleware\HandleMessageMiddleware;

/**
 * Class AddSolarSystemCommand
 *
 * @package AstroLab\SolarSystem\Command
 */
final class AddSolarSystemCommand extends Command
{
    protected static $defaultName = 'astrolab:solar-systems:add';
    private $solarSystemRepository;

    public function __construct(SolarSystemRepository $solarSystemRepository)
    {
        $this->solarSystemRepository = $solarSystemRepository;
        parent::__construct();
    }

    protected function configure()
    {
        // stub
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Add Solar System');
        $helper = $this->getHelper('question');
        $question = new Question('Please enter the name of the Solar System' . PHP_EOL);
        $solarSystemName = $helper->ask($input, $output, $question);
        $handler = new AddSolarSystemHandler($this->solarSystemRepository);
        $bus = new MessageBus(
            [
                new HandleMessageMiddleware(
                    new HandlersLocator(
                        [
                            AddSolarSystemMessage::class => [
                                $handler
                            ],
                        ]
                    )
                ),
            ]
        );
        try {
            $bus->dispatch(new AddSolarSystemMessage($solarSystemName));
            $command = $this->getApplication()
                ->find('astrolab:solar-systems:list');
            $command->run($input, $output);
            return Command::SUCCESS;
        } catch (\Throwable $e) {
            $io->error($e->getMessage());
            return Command::FAILURE;
        }
    }
}
