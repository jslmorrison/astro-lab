<?php

namespace AstroLab\SolarSystem\Command;

use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Command\Command;
use AstroLab\SolarSystem\SolarSystemRepository;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class ListSolarSystemsCommand
 *
 * @package AstroLab\SolarSystem\Command
 */
final class ListSolarSystemsCommand extends Command
{
    protected static $defaultName = 'astrolab:solar-systems:list';
    private $solarSystemRepository;

    public function __construct(SolarSystemRepository $solarSystemRepository)
    {
        $this->solarSystemRepository = $solarSystemRepository;
        parent::__construct();
    }

    protected function configure()
    {
        // stub
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $io->title('Solar Systems');
        $solarSystems = $this->solarSystemRepository
            ->findAll();
        if (!empty($solarSystems)) {
            $rows = [];
            $table = new Table($output);
            $table->setHeaders(['ID', 'Name', 'Mass(kg)']);
            foreach ($solarSystems as $solarSystem) {
                $rows[] = [$solarSystem->id(), $solarSystem->name(), $solarSystem->mass()];
            }
            $table->setRows($rows);
            $table->render();
        }
        if (empty($solarSystems)) {
            $io->writeln('No results, why not add some?');
        }

        return Command::SUCCESS;
    }
}
