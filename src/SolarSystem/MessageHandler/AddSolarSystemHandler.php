<?php

namespace AstroLab\SolarSystem\MessageHandler;

use AstroLab\SolarSystem\SolarSystem;
use AstroLab\SolarSystem\SolarSystemRepository;
use AstroLab\SolarSystem\Message\AddSolarSystemMessage;

/**
 * Class AddSolarSystemHandler
 *
 * @package AstroLab\SolarSystem\MessageHandler
 */
final class AddSolarSystemHandler
{
    private $solarSystemRepository;

    public function __construct(SolarSystemRepository $solarSystemRepository)
    {
        $this->solarSystemRepository = $solarSystemRepository;
    }

    public function __invoke(AddSolarSystemMessage $message)
    {
        // check it doesnt exist already
        $existing = $this->solarSystemRepository
            ->findByName($message->name());
        if ($existing instanceof SolarSystem) {
            throw new \RuntimeException('Sorry Dave. That already exists in the system'); // replace with custom exception
        }
        $solarSystem = SolarSystem::named($message->name());
        $this->solarSystemRepository
            ->save($solarSystem);
    }
}
