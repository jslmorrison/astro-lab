<?php

namespace AstroLab\DataFixtures;

use AstroLab\Planet\Planet;
use AstroLab\SolarSystem\SolarSystem;
use Doctrine\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\FixtureInterface;

/**
 * Class AstroLabFixtures
 *
 * @package AstroLab\DataFixtures
 */
final class AstroLabFixtures implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        echo 'Loading fixtures...' . PHP_EOL;
        $names = [
            'The Solar System',
            'Another Solar System',
            'Far far away',
        ];
        foreach ($names as $name) {
            $solarSystem = SolarSystem::named($name);
            if ($name === 'The Solar System') {
                $planets = [
                    [
                        'name' => 'Mercury',
                        'mass' => 3.14E24,
                        'diameter' => 4878
                    ],
                    [
                        'name' => 'Venus',
                        'mass' => 3.14E24,
                        'diameter' => 12104
                    ],
                    [
                        'name' => 'Earth',
                        'mass' => 5.972E24,
                        'diameter' => 12760
                    ],
                    [
                        'name' => 'Mars',
                        'mass' => 3.14E24,
                        'diameter' => 6787
                    ]
                ];
                foreach ($planets as $planet) {
                    $astroBody = Planet::namedWithMassAndDiameter(
                        $planet['name'],
                        $planet['mass'],
                        $planet['diameter']
                    );
                    $solarSystem->addAstronomicalBody($astroBody);
                }
            }
            $manager->persist($solarSystem);
        }
        $manager->flush();
        echo 'Done' . PHP_EOL;
    }
}
