<?php

namespace AstroLab;

use AstroLab\Identity;

/**
 * Interface AstronomicalBodyInterface
 *
 * @package AstroLab
 */
interface AstronomicalBodyInterface
{
    public function id(): Identity;
    public function name(): string;
    public function mass(): float;
    public function diameter(): float;
}
