<?php

namespace AstroLab;

use AstroLab\SolarSystem\SolarSystem;

/**
 * Class AstronomicalBody
 *
 * @package AstroLab
 */
abstract class AstronomicalBody implements AstronomicalBodyInterface
{
    protected $id;
    protected $name;
    protected $mass;
    protected $diameter;
    protected $solarSystem;

    public static function namedWithMassAndDiameter(
        string $name,
        float $mass,
        float $diameter
    ): self {
        // stub
    }

    public function id(): Identity
    {
        return $this->id;
    }

    public function name(): string
    {
        return $this->name;
    }

    public function mass(): float
    {
        return $this->mass;
    }

    public function diameter(): float
    {
        return $this->diameter;
    }

    public function solarSystem(): SolarSystem
    {
        return $this->solarSystem;
    }

    public function setSolarSystem(solarSystem $solarSystem): void
    {
        $this->solarSystem = $solarSystem;
    }
}
