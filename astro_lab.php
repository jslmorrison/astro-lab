<?php

require_once "bootstrap.php";

use AstroLab\Command\WelcomeCommand;
use Symfony\Component\Console\Application;
use AstroLab\SolarSystem\Command\AddSolarSystemCommand;
use AstroLab\SolarSystem\Command\ListSolarSystemsCommand;

$defaultCommand = new WelcomeCommand();
$application = new Application();
$application->add($defaultCommand);
$application->setDefaultCommand($defaultCommand->getName());
$application->add(
    (new ListSolarSystemsCommand($containerBuilder->get('solarSystemRepository')))
);
$application->add(
    (new AddSolarSystemCommand($containerBuilder->get('solarSystemRepository')))
);

$application->run();
